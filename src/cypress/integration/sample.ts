import _ from 'lodash';
import type { Link, Movie } from 'types';

Cypress._.times(10, () => {
  it('Does not do much!', () => {
    cy.task('getNextLink').then((nextLink) => {
      const link: Link = nextLink as Link;
      cy.visit(`https://imdb.com/title/${link.imdbId}`);
      const movie: Partial<Movie> = {};
      cy.findByTestId('hero-title-block__title').should((el) => {
        if (el?.text) {
          movie.name = el.text();
          movie.year = Number(
            el.get(0).nextSibling?.textContent?.substring(0, 4)
          );
          movie.imdbId = link.imdbId;
          cy.task('createMovie', movie);
        }
      });
      cy.get('a').then((res) => {
        let newRes = res.filter((each) => {
          const link = res.get(each).href;
          return typeof link === 'string' && link.indexOf('/title/tt') > -1;
        });
        newRes = _.uniq(newRes, (each: any) => {
          return each.href.split('?')[0];
        });

        const uniqueIds: string[] = [];
        newRes = newRes.filter((each: any) => {
          const tt = each.href.split('/');
          let isUnique = false;
          tt.forEach((e: any) => {
            if (e.startsWith('tt') && uniqueIds.indexOf(e) < 0) {
              uniqueIds.push(e);
              isUnique = true;
            }
          });
          return isUnique;
        });

        (newRes as any).forEach((each: any) => {
          const tt = each.href.split('/');
          tt.forEach((e: any) => {
            const link: Partial<Link> = {};
            if (e.startsWith('tt')) {
              link.imdbId = e;
            }
            link.url = each.href;
            link.visited = false;
            cy.task('createLink', link);
          });
        });
        cy.task('markLinkAsVisited', link);
      });
    });
    // }
  });
});
