/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

import db from 'db/db';
import 'db/movie';
import 'db/link';
import type { Movie, Link } from 'types';
/**
 * @type {Cypress.PluginConfig}
 */
// eslint-disable-next-line no-unused-vars
const defaultExport = async (on, config) => {
  await db.authenticate().then(() => console.log('DB Connected'));
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config
  on('task', {
    async createMovie(movie: Movie) {
      const res = await db.models.Movie.findOne({
        where: { imdbId: movie.imdbId },
      });
      if (!res) {
        const m = await db.models.Movie.build(movie).save();
        return m;
      }
      return null;
    },
    async getNextLink() {
      const res = await db.models.Link.findOne({ where: { visited: 0 } });
      return res;
    },
    async createLink(link: Link) {
      if (link.imdbId) {
        const res = await db.models.Link.findOne({
          where: { imdbId: link.imdbId },
        });
        if (!res) {
          const m = await db.models.Link.build(link).save();
          return m;
        }
      }
      return null;
    },
    async markLinkAsVisited(link: Link) {
      const res = await db.models.Link.findOne({
        where: { imdbId: link.imdbId },
      });
      if (res) {
        // @ts-ignore
        res.visited = true;
        await res.save();
        return res;
      }
      return null;
    },
  });
};

export default defaultExport;
