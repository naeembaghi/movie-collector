import dotenv from 'dotenv';
import path from 'path';
dotenv.config({ path: path.resolve(__dirname, '../.env') });
import request from 'request-promise';
import db from './db/db';
import { JSDOM } from 'jsdom';
import { queryByTestId, prettyDOM } from '@testing-library/dom';
import './db/movie';
import './db/link';

db.authenticate().then(() => console.log('DB Connected'));
// console.log(db.models);

// db.models.Movie.build({ name: 'test', description: 'asdfadf' }).save();

const handler = async () => {
  const res = await request(
    'https://www.imdb.com/title/tt5433138/?ref_=ttnw_hd'
  );
  const doc = new JSDOM(res);

  // console.log(res);
  // const list = doc.window.document.querySelectorAll('a');
  // list.forEach((each) => {
  //   console.log(each.href);
  // });
  // within(doc.window.document as any);
  console.log(
    // prettyDOM(
    doc.window.document.querySelector('[class^="OriginalTitle"]') as any
    // 10000000000000
    // )
  );
  // console.log(
  //   queryByTestId(
  //     doc.window.document as any,
  //     'hero-title-block__original-title'
  //   )
  // );
  // console.log('length: ', list?.length);
};

// handler();

db.models.Link.findAll({ where: { visited: 0 } }).then(console.log);
