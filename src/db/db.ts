import { Sequelize } from 'sequelize';
import accessEnv from '../utils/accessEnv';

const DB_URL = accessEnv('DB_URL');

const sequelize = new Sequelize(DB_URL, {
  dialectOptions: {
    charset: 'utf8',
    multipleStatements: true,
  },
  logging: true,
  dialect: 'mysql',
  define: {
    paranoid: true,
    underscored: true,
    freezeTableName: true,
  },
});

export default sequelize;
