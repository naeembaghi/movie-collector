import { Model, DataTypes } from 'sequelize';
import sequelize from './db';

class Link extends Model {}

Link.init(
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    url: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    visited: {
      type: DataTypes.TINYINT,
      defaultValue: 0,
      allowNull: true,
    },
    imdbId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  { sequelize, tableName: 'link' }
);

export default Link;
