export type Link = {
  id: number;
  url: string;
  visited: boolean;
  imdbId: string;
  createdAt: string;
  updatedAt: string;
  deletedAt?: string;
};
