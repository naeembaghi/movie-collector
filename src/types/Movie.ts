export type Movie = {
  name: string;
  year: number;
  imdbId: string;
};
